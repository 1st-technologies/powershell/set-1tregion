<#
.SYNOPSIS
    Configure region.
.DESCRIPTION
    Configure regional settings based on AD site.
.PARAMETER LocationFile
    Specifies the json file with regional settings.
.LINK
    https://gitlab.com/1st-technologies/powershell/set-1tregion
.INPUTS
    None. Set-1TRegion does not accept object inputs.
.OUTPUTS
    None, Set-1TRegion does not output any objects.
.EXAMPLE
    C:\ PS> .\Set-1TRegion.ps1
    Configures all region specific information depending on AD Site and settings as defined  the 'locations.json' file in the current directory.
.EXAMPLE
    C:\ PS> .\Set-1TRegion.ps1 -LocationFile .\molocationdata.json -Site Paris
    Configures all region specific information for Paris as defined in '.\mylocationdata.json' file.
.NOTES
    Name     : Set-1TRegion
    Version  : 0.1.0
    Author   : 1st Technologies, Inc.
    Date     : February 20, 2022
    Copyright: (c) 1st Technologies, Inc. All Rights Reserved
    Changes  : 0.1.0 - Initial Release
               0.1.1 - add ad site check
#>

#Requires -Version 4.0
##Requires -RunAsAdministrator

[CmdletBinding(SupportsShouldProcess=$true)]
Param(
    [Parameter(Mandatory = $false, HelpMessage = "Specifies the location data file.")]
    [ValidateScript({
        If(Test-Path -Path $_ -PathType Leaf){
            $true
        }else{
            Throw "Location data file not found: $_"
        }
    })]
    [ValidateNotNullOrEmpty()]
    [string]$LocationFile = "$PSScriptRoot\locations.json",
    [Parameter(Mandatory = $false, HelpMessage = "Manually specify a site, overriding AD site detection.")]
    [ValidateNotNullOrEmpty()]
    [string]$Site
)


Begin{
    Function Set-1TIntl{
        [CmdletBinding(SupportsShouldProcess=$true)]
        Param(
            [Parameter(Mandatory = $true, HelpMessage = "Specifies the primary lanaguage")]
            [ValidateSet("en-AU", "en-IE", "en-US", "es-ES", "fr-FR", "ja-JP")]
            [string]$PrimaryLanguage,
            [Parameter(Mandatory = $false, HelpMessage = "Optionally specifies the fallback lanaguage")]
            [ValidateSet("en-US")]
            [string]$FallbackLanguage,
            [Parameter(Mandatory = $true, HelpMessage = "Specifies the user culture settings")]
            [ValidateSet("en-AU", "en-IE", "en-US", "es-ES", "fr-FR", "ja-JP")]
            [string]$UserCulture,
            [Parameter(Mandatory = $true, HelpMessage = "Specifies the system culture settings ")]
            [ValidateSet("en-AU", "en-IE", "en-US", "es-ES", "fr-FR", "ja-JP")]
            [string]$SystemCulture,
            [Parameter(Mandatory = $true, HelpMessage = "Specifies the geographical location code")]
            [ValidatePattern('\d+')]
            [string]$GeoCode,
            [Parameter(Mandatory = $true, HelpMessage = "Specifies keyboard layouts to add")]
            [ValidateNotNullOrEmpty()]
            [string[]]$AddKeyboards,
            [Parameter(Mandatory = $false, HelpMessage = "Optionally specifies keyboard layouts to remove")]
            [string[]]$RemoveKeyboards
        )

        Write-Verbose -Message "Cheking if required language packs are installed..."
        $InstalledLanguages = (Get-CimInstance -ClassName win32_operatingsystem -Property MUILanguages).MUILanguages
        If($InstalledLanguages -notcontains $PrimaryLanguage){
            Throw "ERROR: Language pack $PrimaryLanguage not installed"
        }

        If($FallbackLanguage){
            If($InstalledLanguages -notcontains $FallbackLanguage){
                Throw "ERROR: Language pack $FallbackLanguage not installed"
            }
        }


        Write-Verbose -Message "Building Intl XML File..."
        $objXMLFile = New-TemporaryFile
        $XMLFile = $objXMLFile.FullName

        $mXMLSettings = New-Object System.Xml.XmlWriterSettings
        $mXMLSettings.OmitXmlDeclaration = $true

        $Prefix = "gs"
        $Ns = "urn:longhornGlobalizationUnattend"
        
        [System.XML.XmlWriter]$mXML = [System.XML.XmlWriter]::Create($XmlFile, $mXMLSettings)
        
        $mXML.WriteStartDocument()
        $mXML.WriteStartElement($Prefix, "GlobalizationServices", $Ns)

        $mXML.WriteStartElement($Prefix, "UserList", $Ns)
        $mXML.WriteStartElement($Prefix, "User", $Ns)
        $mXML.WriteAttributeString("UserID", "Current")
        $mXML.WriteAttributeString("CopySettingsToDefaultUserAcct", "true")
        $mXML.WriteAttributeString("CopySettingsToSystemAcct", "true")
        $mXML.WriteEndElement()
        $mXML.WriteEndElement()

        $mXML.WriteStartElement($Prefix, "MUILanguagePreferences", $Ns)
        $mXML.WriteStartElement($Prefix, "MUILanguage", $Ns)
        $mXML.WriteAttributeString("Value", $PrimaryLanguage)
        $mXML.WriteEndElement()
        
        If($FallbackLanguage){
            $mXML.WriteStartElement($Prefix, "MUIFallback", $Ns)
            $mXML.WriteAttributeString("Value", $FallbackLanguage)
            $mXML.WriteEndElement()
        }
        
        $mXML.WriteEndElement()

        $mXML.WriteStartElement($Prefix, "UserLocale", $Ns)
        $mXML.WriteStartElement($Prefix, "Locale", $Ns)
        $mXML.WriteAttributeString("Name", $UserCulture)
        $mXML.WriteAttributeString("SetAsCurrent", "true")
        $mXML.WriteEndElement()
        $mXML.WriteEndElement()

        $mXML.WriteStartElement($Prefix, "SystemLocale", $Ns)
        $mXML.WriteAttributeString("Name", $SystemCulture)
        $mXML.WriteEndElement()
        
        $mXML.WriteStartElement($Prefix, "LocationPreferences", $Ns)
        $mXML.WriteStartElement($Prefix, "GeoID", $Ns)
        $mXML.WriteAttributeString("Value", $GeoCode)
        $mXML.WriteEndElement()
        $mXML.WriteEndElement()

        $mXML.WriteStartElement($Prefix, "InputPreferences", $Ns)
        $aDefault = $true
        ForEach($AddKeyboard In $AddKeyboards){
            $mXML.WriteStartElement($Prefix, "InputLanguageID", $Ns)
            $mXML.WriteAttributeString("Action", "add")
            $mXML.WriteAttributeString("ID", $AddKeyboard)
            If($aDefault){
                $mXML.WriteAttributeString("Default", 'true')
            }
            $mXML.WriteEndElement()
            $aDefault = $false
        }

        If($RemoveKeyboards.count -gt 0){
            ForEach($RemoveKeyboard In $RemoveKeyboards){
                $mXML.WriteStartElement($Prefix, "InputLanguageID", $Ns)
                $mXML.WriteAttributeString("Action", "remove")
                $mXML.WriteAttributeString("ID", $RemoveKeyboard)
                $mXML.WriteEndElement()
            }
            $mXML.WriteEndElement()
        }
        $mXML.WriteEndElement()

        $mXML.WriteEndDocument()

        $mXML.Flush()
        $mXML.Close()

        Write-Verbose -Message "Applying Legacy Intl Settings..."
        Set-WinLanguageBarOption -UseLegacySwitchMode:$true -UseLegacyLanguageBar:$true
        If ($PSCmdlet.ShouldProcess("Local System", "Apply legacy regional settings for $ADSite")){
            Start-Process -FilePath "C:\Windows\system32\control.exe" -ArgumentList "intl.cpl,, /f:'$XMLFile'" -Wait
        }
        Start-Sleep -Seconds 3
        Set-WinLanguageBarOption -UseLegacySwitchMode:$false -UseLegacyLanguageBar:$false

        Write-Debug -Message "XML File:`r`n$(Get-content -Path $XMLFile)"

        Write-Verbose -Message "Deleting Intl XML file..."
        Remove-Item -Path $XMLFile

        Write-Verbose -Message "Applying Win10/Tablet Intl Settings..."
        if ($PSCmdlet.ShouldProcess("Local System", "Apply regional settings for $ADSite ")){
            Set-WinSystemLocale -SystemLocale $SystemCulture
            Set-WinHomeLocation -GeoId $GeoCode
            Set-WinDefaultInputMethodOverride -InputTip $AddKeyboards[0]
            Set-Culture -CultureInfo $UserCulture
            
            Set-WinUILanguageOverride -Language $PrimaryLanguage

            If(($UserLanguages| Select-Object -ExpandProperty LanguageTag) -contains "ja-JP" -eq $false){
                $UserLanguages = Get-WinUserLanguageList
                $UserLanguages.Add($PrimaryLanguage)
                Set-WinUserLanguageList -LanguageList $UserLanguages -Force
            }
        }
    }
}


Process{
    Write-Verbose -Message "Reading locations data file..."
    $LocationData = Get-Content -Path $LocationFile | ConvertFrom-Json

    If($Site){
        Write-Verbose -Message "Manual site specified"
        If($LocationData.$Site){
            $ADSite = $Site
        }else{
            Throw "Error: Site '$Site' not defined in $LocationFile"
        }
    }else{
        Write-Verbose -Message "Getting Active Directory Site..."
        Try{
            $ADSite = [System.DirectoryServices.ActiveDirectory.ActiveDirectorySite]::GetComputerSite().Name
        }Catch{
            Throw("Error getting Active Directory Site: {0}" -f $($_.exception.message)).ToString().Trim()
        }
        Write-Verbose -Message "AD Site is $ADSite"
    }

    Write-output "Site              : $ADSite"
    Write-output "Primary Language  : $($LocationData.$ADSite.primaryLanguage)"
    Write-output "Fallback Language : $($LocationData.$ADSite.fallbackLanguage)"
    Write-output "Culture           : $($LocationData.$ADSite.culture)"
    Write-output "Geo Code          : $($LocationData.$ADSite.locationCode)"
    Write-output "Add Keyboards     : $($LocationData.$ADSite.addKeyboards)"
    Write-output "Remove Keyboards  : $($LocationData.$ADSite.removeKeyboards)"
    Write-output "Time Zone         : $($LocationData.$ADSite.timeZone)"

    Write-Verbose -Message "Setting Legacy Region and Culture..."
    if($LocationData.$ADSite.fallbackLanguage -and $LocationData.$ADSite.RemoveKeyboards){
        Set-1TIntl `
            -PrimaryLanguage $LocationData.$ADSite.primaryLanguage `
            -UserCulture $LocationData.$ADSite.culture `
            -SystemCulture $LocationData.Value.culture `
            -GeoCode $LocationData.$ADSite.locationCode `
            -AddKeyboards $LocationData.$ADSite.addKeyboards `
            -FallbackLanguage $LocationData.$ADSite.fallbackLanguage `
            -RemoveKeyboards $LocationData.$ADSite.removeKeyboards
    }elseif(!($LocationData.$ADSite.fallbackLanguage) -and $LocationData.$ADSite.RemoveKeyboards){
        Set-1TIntl `
            -PrimaryLanguage $LocationData.$ADSite.primaryLanguage `
            -UserCulture $LocationData.$ADSite.culture `
            -SystemCulture $LocationData.$ADSite.culture `
            -GeoCode $LocationData.$ADSite.locationCode `
            -AddKeyboards $LocationData.$ADSite.addKeyboards `
            -RemoveKeyboards $LocationData.$ADSite.removeKeyboards
    }elseif($LocationData.$ADSite.fallbackLanguage -and !($LocationData.$ADSite.RemoveKeyboards)){
        Set-1TIntl `
            -PrimaryLanguage $LocationData.$ADSite.primaryLanguage `
            -UserCulture $LocationData.$ADSite.culture `
            -SystemCulture $LocationData.$ADSite.culture `
            -GeoCode $LocationData.$ADSite.locationCode `
            -AddKeyboards $LocationData.$ADSite.addKeyboards `
            -FallbackLanguage $LocationData.$ADSite.fallbackLanguage
    }elseif(!($LocationData.$ADSite.fallbackLanguage) -and !($LocationData.$ADSite.RemoveKeyboards)){
        Set-1tIntl `
            -PrimaryLanguage $LocationData.$ADSite.primaryLanguage `
            -UserCulture $LocationData.$ADSite.culture `
            -SystemCulture $LocationData.$ADSite.culture `
            -GeoCode $LocationData.$ADSite.locationCode `
            -AddKeyboards $LocationData.$ADSite.addKeyboards
    }

    Write-Verbose -Message "Setting Time Zone..."
    Set-TimeZone -Id $LocationData.$ADSite.timeZone
}


End{
    Write-Output "Done."
    exit 3010
}

