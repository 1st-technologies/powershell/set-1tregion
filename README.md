[[_TOC_]]

# Usage

`Set-1TRegion.ps1 [[-LocationFile] <String>] [[-Site] <String>] [-WhatIf] [-Confirm] [<CommonParameters>]`

## Parameters

    -LocationFile <String>
        Specifies the json file with regional settings.

        Required?                    false
        Position?                    1
        Default value                "$PSScriptRoot\locations.json"
        Accept pipeline input?       false
        Accept wildcard characters?  false

    -Site <String>

        Required?                    false
        Position?                    2
        Default value
        Accept pipeline input?       false
        Accept wildcard characters?  false

    -WhatIf [<SwitchParameter>]

        Required?                    false
        Position?                    named
        Default value
        Accept pipeline input?       false
        Accept wildcard characters?  false

    -Confirm [<SwitchParameter>]

        Required?                    false
        Position?                    named
        Default value
        Accept pipeline input?       false
        Accept wildcard characters?  false

    <CommonParameters>
        This cmdlet supports the common parameters: Verbose, Debug,
        ErrorAction, ErrorVariable, WarningAction, WarningVariable,
        OutBuffer, PipelineVariable, and OutVariable. For more information, see
        about_CommonParameters (https:/go.microsoft.com/fwlink/?LinkID=113216).

## Examples

    -------------------------- EXAMPLE 1 --------------------------

    PS C:\>.\Set-1TRegion.ps1

    Configures all region specific information depending on AD Site and settings as defined in 'locations.json' file
    in the current directory.


    -------------------------- EXAMPLE 2 --------------------------

    PS C:\>.\Set-1TRegion.ps1 -LocationFile .\mylocationdata.json -Site Paris

    Configures all region specific information for Paris as defined in the '.\mylocationdata.json' file.

# Settings File

The locations.json file is organized into an array of sites with a nested arrays of region properties.

Each root element is named to match an existing Active Directory site name. Corresponding values are nested below.

## Locations File Schema

```JSON
{
	"<string>AD SITE NAME": {
		"primaryLanguage": "<string>LANGUAGE CODE, e.g. 'en-GB'",
		"fallbackLanguage": "<string>LANGUAGE CODE, e.g. 'en-US'",
		"addKeyboards": [
			"<string>KEYBOARD ID OR GUID, e.g. 0409:00000409 or (0804:{81D4E9C9-1D3B-41BC-9E6C-4B40BF79E35E}",
            ...
		],
		"removeKeyboards": [
			"<string>KEYBOARD ID OR GUID, e.g. 0409:00000409 or (0804:{81D4E9C9-1D3B-41BC-9E6C-4B40BF79E35E}",
            ...
		],
		"culture": "<string>LANGUAGE CODE, e.g. 'en-GB'",
		"locationCode": "<int>GEO CODE, e.g. 244",
		"timeZone": "<string>TIME ZONE NAME, e.g. 'Eastern Standard Time'"
	},
    ...
}
```

## Site properties

### primaryLanguage

Enter the desired the primary language of the system.

### fallbackLanguage

If the primary language is only "partially localized" [here](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-8.1-and-8/hh825678(v=win.10)?redirectedfrom=MSDN), enter the base language here.

### addKeyboards

An array of keyboard identifiers to add as listed [here](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-8.1-and-8/hh825682(v=win.10)?redirectedfrom=MSDN)

### removeKeyboards

An array of keyboard identifiers to remove as listed [here](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-8.1-and-8/hh825682(v=win.10)?redirectedfrom=MSDN)

It is only recommended to remove US english keyboard where the primary language is another English dialect (e.g. en-GB, en-CA, en-AU, etc).

### locationCode

Set the geographical code from values listed [here](https://docs.microsoft.com/en-us/previous-versions/windows/embedded/ms912389(v=winembedded.11)?redirectedfrom=MSDN).

### timeZone

The time zone string as listed [here](https://docs.microsoft.com/en-us/previous-versions/windows/embedded/ms912391(v=winembedded.11)?redirectedfrom=MSDN)


## Included settings file

```JSON
{
	"Sydney": {
		"primaryLanguage": "en-AU",
		"fallbackLanguage": "en-US",
		"addKeyboards": [
			"0c09:00000409"
		],
		"removeKeyboards": [
			"0409:00000409"
		],
		"culture": "en-AU",
		"locationCode": 12,
		"timeZone": "AUS Eastern Standard Time"
	},
	"Tokyo": {
		"primaryLanguage": "ja-JP",
		"fallbackLanguage": "",
		"addKeyboards": [
			"0411:{03B5835F-F03C-411B-9CE2-AA23E1171E36}{A76C93D9-5523-4E90-AAFA-4DB112F9AC76}"
		],
		"removeKeyboards": [],
		"culture": "ja-JP",
		"locationCode": 122,
		"timeZone": "Tokyo Standard Time"
	},
	"Dublin": {
		"primaryLanguage": "en-IE",
		"fallbackLanguage": "en-US",
		"addKeyboards": [
			"1809:00001809"
		],
		"removeKeyboards": [
			"0409:00000409"
		],
		"culture": "en-IE",
		"locationCode": 68,
		"timeZone": "GMT Standard Time"
	},
	"Paris": {
		"primaryLanguage": "fr-FR",
		"fallbackLanguage": "",
		"addKeyboards": [
			"040c:0000040c"
		],
		"removeKeyboards": [],
		"culture": "fr-FR",
		"locationCode": 84,
		"timeZone": "Romance Standard Time"
	}
}
```

# Additional reading

* [Windows Language Pack Default Values](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-8.1-and-8/hh825682(v=win.10)?redirectedfrom=MSDN)
* [Available Language Packs for Windows](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-8.1-and-8/hh825678(v=win.10)?redirectedfrom=MSDN)
* [Microsoft Geographical Location Values](https://docs.microsoft.com/en-us/previous-versions/windows/embedded/ms912389(v=winembedded.11)?redirectedfrom=MSDN)
* [Microsoft Time Zone Index Values](https://docs.microsoft.com/en-us/previous-versions/windows/embedded/ms912391(v=winembedded.11)?redirectedfrom=MSDN)
* [Guide to Windows Vista Multilingual User Interface](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-vista/cc721887(v=ws.10)?redirectedfrom=MSDN/)